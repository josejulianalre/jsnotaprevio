
function calcular() {
    //leer datos

    let nombre = document.getElementById("nombre").value;
    let pt = parseFloat(document.getElementById("pt").value);
    let pp = parseFloat(document.getElementById("pp").value);
    let bonus = parseFloat(document.getElementById("bp").value);
    let notaP = (pt * 0.4) + (pp * 0.6) + bonus;

    //Calcular datos
    let titulo = "Nota";
    let nomb = nombre;
    let rta = " Su nota del previo 1 es :" + notaP;

    //mostrar datos
    let card = document.getElementById("card");
    let tit = document.getElementById("titulo");
    let nom = document.getElementById("nombr");
    let respuesta = document.getElementById("respuesta");

    tit.innerHTML = titulo;
    nom.innerHTML = nomb;
    respuesta.innerHTML = rta;

}

function calcularPrevio(){

    let previo = document.getElementsByName("previo");
        //alert("Contiene : " +previ.length);
        let respuesta = document.getElementById("respuesta");
    if(!validar(previo))
    {
        respuesta.innerHTML= "<b>Error de alguna nota</b>";
    }
    else
    {
       // respuesta.innerHTML= "<b>Procesando nota </b>";
        let notaP =calculaNota(previo);
        respuesta.innerHTML= "<b>Su nota promedio es :"+notaP+" </b>";
        crearTabla(previo, notaP);
        //crearGrafico(previo, notaP);
    }

}
function validar(previo)
{
    for(elemento of previo)
    {
        if(elemento.getAttribute("type")==="number")
        {
            let valor = parseFloat(elemento.value);
            let min = parseInt(elemento.getAttribute("min"));
            let max = parseInt(elemento.getAttribute("max"));
            if (valor < min || valor > max)
            {
                elemento.style.color="red";
                return false;
            }
        }
    }
    return true;
}

function calculaNota(previo) {

    let pt = parseFloat(previo[1].value);
    let pp = parseFloat(previo[2].value);
    let bonus = parseFloat(previo[3].value);
    let notaP = (pt * 0.4) + (pp * 0.6) + bonus;
    return notaP;
    
}



